<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use MaddHatter\LaravelFullcalendar\Facades\Calendar;
class EventController extends Controller
{
       public function index()
            {
                $events = [];
                $data = Event::all();
                if($data->count()) {
                    foreach ($data as $key => $value) {
                        $events[] = Calendar::event(
                            $value->title,
                            /*Fullday*/false,
                            new \DateTime($value->start_date),
                            new \DateTime($value->end_date),
                            null,
                            // Add color and link on event
                         [
                             'color' => '#ff0000',
                             'url' => 'pass here url and any route',
                         ]
                        );
                    }
                }
                $calendar = Calendar::addEvents($events)->setOptions([
                    //set fullcalendar options
                    // 'header' => [
                    //     "left" =>  "title",
                    //     "center" => "",
                    //     "right" => "month, listDay, listWeek, listMonth, agenda"
                    // ],
                    'nowIndicator' => true,
                    'allDaySlot' => false,
                    'minTime' => "08:00:00",
                    'maxTime' => "20:00:00",
                    'contentWidth' => "auto",
                    'axisFormat' => 'H:mm',
                ]);
                return view('/vendor/voyager/tour/fullcalendar', compact('calendar'));
            }
}