<?php

use Illuminate\Database\Seeder;

class AddDummyEvent extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
          $data = [
            ['title'=>'Demo Event-1', 'start_date'=>'2019-02-12T14:00:00', 'end_date'=>'2019-02-12T15:00:00'],
        	['title'=>'Demo Event-2', 'start_date'=>'2019-02-13T14:00:00', 'end_date'=>'2019-02-14T14:00:00'],
        	['title'=>'Demo Event-3', 'start_date'=>'2019-02-20T02:20:00', 'end_date'=>'2019-02-21T10:00:00'],
        ];
        \DB::table('events')->insert($data);
    }
}